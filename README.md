# Projet WebServices

## Création d'une plateforme de recrutement 

## Clipet Mathilde - Mathon Rémi

## React <img src="https://upload.wikimedia.org/wikipedia/commons/thumb/a/a7/React-icon.svg/2300px-React-icon.svg.png" alt="react Logo" width="20"/> - Express <img src="https://cdn.pixabay.com/photo/2015/04/23/17/41/javascript-736400_960_720.png" alt="express Logo" width="20"/>

## BDD - MongoDB <img src="https://1000logos.net/wp-content/uploads/2020/08/MongoDB-Emblem.jpg" alt="express Logo" width="20"/>
- La connexion à la BDD dans /Repository/app.js

>   const MongoClient = require('mongodb').MongoClient;
>   const url = 'mongodb+srv://root:root@ws.mqaoo.mongodb.net/WS?retryWrites=true&w=majority;'

#### Serveur dans /Repository
- Les web services sont dans /Repository/routes

#### Client dans /Presentation
- Utilisation des web services dans /Presentation/src


### Consignes
 - Notation = Services Web REST
 - Production de WS (Get Post Put Delete)
 - Utilisation de WS (Interne et externe)
 - Somme de données (BDD et/ou sources externes)
 - Choix du thème
 - Graph QL (Bonus facultatif)
 - Choix du framework
 
 
 ## Web Services
 
 ### Authentification 
 
>  <font color='green'>[POST]</font> http://localhost:8080/users/singup
>
> Enregistre un utilisateur 
>
    {
        "username": "mthremi@gmail.com",
        "password": "monmotdepasse",
        "firstName": "Rémi",
        "lastName": "Mathon"
   	}
   	
   	
Return --> Token

-------------
> <font color='green'>[POST]</font> http://localhost:8080/users/login
>
> Connecte un utilisateur
>
    {
        "username": "mthremi@gmail.com",
        "password": "monmotdepasse"
   	}
Return -> Token

-------------
> <font color='green'>[POST]</font> http://localhost:8080/users/refreshToken
>
> Refresh le token utilisateur
>
    {
        "token": "token",
   	}
Return -> new Token

-------------
> <font color='skyblue'>[GET]</font> http://localhost:8080/users/me
>
> Confirme la connexion d'un user
>
    {
        "token": "token",
   	}
Return 
>

    {
        "_id": "624ed34ae7713943b06bcf8c",
        "firstName": "Rémi",
        "lastName": "Mathon",
        "role": "user",
        "password": "local",
        "username": "mthremi@gmail.com"
    }
    
    
-------------
> <font color='skyblue'>[GET]</font> http://localhost:8080/users/logout
>
> Met fin à la session d'un utilisateur 
>
    {
        "token": "token",
   	}
Return 
>

    {
        "success": true
    }

-------------
> <font color='red'>[DELETE]</font> http://localhost:8080/users/delete/:id
>
> Supprime l'utilisateur [ID]
>

Return 
>

    {
    	"success": true
    }
    
    
-------------
> <font color='orange'>[PUT]</font> http://localhost:8080/users/newPassword
>
> Modifie le mot de passe de l'utilisateur connecté 
>

    {
        "newPassword": "nouveaumotdepasse"
    }

Return 
>

    {
    	"success": true
    }



 ### Publications 
 
>  <font color='green'>[POST]</font> http://localhost:8080/publications/new
>
> Enregistre une publication
>
    {
        "title": "Ma Nouvelle Publication",
        "description": "Ceci est une description",
        "path": "/path/to/my/file"
    }
   	
   	
Return --> Json{Success + Publication}

-------------

>  <font color='shyblue'>[GET]</font> http://localhost:8080/publications
>
> Return toutes les publications
>  	
   	
Return --> Liste de publications

-------------

>  <font color='red'>[DELETE]</font> http://localhost:8080/publications/delete/:id
>
> Delete la publication 
>  	
   	
    {
    	"success": true
    }

-------------


>  <font color='orange'>[PUT]</font> http://localhost:8080/publications/update/:id
>
> Modifie la publication
>  	
>
>
    {
        "title": "Ma Nouvelle Publication",
        "description": "Ceci est une description",
        "path": "/path/to/my/file"
    }
Return 

    {
    	"success": true
    }

-------------


>  <font color='SKYBLUE'>[GET]</font> http://localhost:8080/publications/:id
>
> Return la publication
>  	
>
>

Return --> publication


-------------



## Organisation
 - Presentation : partie présentation du n-tier, contient le react
 - Business : partie Business du n-tier, la zone de traitement
 - Repository : partie Data du n-tier contient les appels au données mongodb

## Lancement
- Se rendre dans Presentation pour mettre en route le front
- npm start
- Se rendre dans Repository pour mettre en route le back

