const mongoose = require("mongoose")
const Schema = mongoose.Schema


const Publication = new Schema({
    title: {
        type: String,
        default: "",
    },
    description: {
        type: String,
        default: "",
    },
    date: {
        type: Date,
        default: Date.now,
    },
    path: {
        type: String,
        default: "unknown",
    },
    user: {
        type: String
    }
})



module.exports = mongoose.model("Publication", Publication)
