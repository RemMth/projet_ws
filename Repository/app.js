var createError = require('http-errors');
var express = require('express');
var path = require('path');
const cors = require("cors")
const bodyParser = require("body-parser")
const cookieParser = require("cookie-parser")
var logger = require('morgan');
const passport = require("passport")
var indexRouter = require('./routes/index');
require("./strategies/JwtStrategy")
require("./strategies/localStrategy")
require("./authenticate")

const userRouter = require("./routes/userRoutes")
const publicationRouter = require("./routes/publicationRoutes")
var app = express();
require("./utils/db_connect");

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'pug');

/**
 * Import MongoClient & connexion à la DB
 */
const MongoClient = require('mongodb').MongoClient;
const url = 'mongodb+srv://root:root@ws.mqaoo.mongodb.net/WS?retryWrites=true&w=majority;'
let db;

MongoClient.connect(url, function(err, db) {
  if (err) throw err;
  dbase = db.db("WS");
  console.log("Switched to "+dbase.databaseName+" database");
});

/*const corsOptions = {
  origin: function (origin, callback) {
  if (!origin || whitelist.indexOf(origin) !== -1) {
    callback(null, true)
  } else  {
    callback(new Error("Not allowed by CORS"))
  }
  },
  credentials: true,
}*/



app.use(express.json())

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser(process.env.COOKIE_SECRET))
app.use(bodyParser.json())
app.use(express.static(path.join(__dirname, 'public')));
//app.use(cors(corsOptions))
app.use(cors())
app.use(passport.initialize())

app.use('/', indexRouter);
app.use("/users", userRouter);
app.use("/publications", publicationRouter);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

module.exports = app;
