const express = require("express")
const router = express.Router()
const Publication = require("../models/publication")
const jwt = require("jsonwebtoken")
const {
    getToken,
    COOKIE_OPTIONS,
    getRefreshToken,
    verifyUser, } = require("../authenticate")
    var ObjectID = require('mongodb').ObjectID;

router.post("/new", verifyUser, (req, res, next) => {
    if (!req.body.title) {
        res.statusCode = 500
        res.send({
            name: "TitleError",
            message: "The title is required",
        })
    }
    if (!req.body.description) {
        res.statusCode = 500
        res.send({
            name: "DescriptionError",
            message: "The description is required",
        })
    }
    const publication = new Publication({
        title: req.body.title,
        description: req.body.description,
        date: Date.now(),
        path: req.body.path,
        user: req.user,
    })
    publication.save((err, publication) => {
        if (err) {
            res.statusCode = 500
            res.send(err)
        } else {
            res.send({ success: true, publication })
        }
    })
})


router.get("/", verifyUser, (req, res, next) => {
    Publication.find({}).then(
        publications => {
            res.send({ publications })
        }
    )
})

router.delete("/delete/:id", verifyUser, (req, res, next) => {
    Publication.findByIdAndRemove(req.params.id).then(
        publication => {
            res.send({ publication })
        }
    )
})


router.put("/update/:id", verifyUser, (req, res, next) => {
    Publication.findByIdAndUpdate(req.params.id, req.body).then(
        publication => {
            publication.title = req.body.title
            publication.description = req.body.description
            publication.path = req.body.path
            publication.save((err, publication) => {
                if (err) {
                    res.statusCode = 500
                    res.send(err)
                } else {
                    res.send({ success: true, publication })
                }
            }
            )
            


        }
    )
})



router.get("/:id", verifyUser, (req, res, next) => {
    Publication.findById(req.params.id).then(
        publication => {
            res.send({ publication })
        }
    )
})


module.exports = router;
