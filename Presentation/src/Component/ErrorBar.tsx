import * as React from 'react';
import Snackbar from '@mui/material/Snackbar';
import MuiAlert, { AlertProps } from '@mui/material/Alert';



class ErrorBar extends React.Component<any,any>{

    constructor(props:any){
        super(props);
        this.state = {
        }
        this.handleClose = this.handleClose.bind(this);   
    }


    handleClose = (event?: React.SyntheticEvent | Event, reason?: string) => {
        if (reason === 'clickaway') {
          return;
        }
        this.props.stopMessage(false);
      };
    
    render(){
        return(
            <>
                {this.props.snackMessage.map((item:String,i:any) =>{
                    return (
                        /*<Snackbar open={true} autoHideDuration={6000} onClose={this.handleClose} key={i}>
                            <div className='col-6 bg-danger rounded bg-gradient fs-5 bg-opacity-75 mt-2 white'>{item}</div>
                        </Snackbar>*/          
                        <div className='row' key={i}>
                            <div className='col-3'></div>
                            <div className='col-6 bg-danger rounded fs-5 bg-opacity-75 mt-2 white w-25 mx-auto text-center' >Erreur : {item}</div>
                            <div className='col-3'></div>
                        </div>
                    )}
                )}
            </>
        );
    }
}
export default ErrorBar;