import * as React from 'react';
import AppBar from '@mui/material/AppBar';
import Box from '@mui/material/Box';
import Toolbar from '@mui/material/Toolbar';
import Typography from '@mui/material/Typography';
import IconButton from '@mui/material/IconButton';
import MenuIcon from '@mui/icons-material/Menu';
import AccountCircle from '@mui/icons-material/AccountCircle';
import Switch from '@mui/material/Switch';
import FormControlLabel from '@mui/material/FormControlLabel';
import FormGroup from '@mui/material/FormGroup';
import MenuItem from '@mui/material/MenuItem';
import Menu from '@mui/material/Menu';
import DrawerMenu from './DrawerMenu';
import * as authent from '../Services/AuthentificationAPI.js';
import LogoutIcon from '@mui/icons-material/Logout';

interface MyProps {
    openDrawer:boolean
}
  
interface MyState {
    auth:boolean,
    anchorEl:null | HTMLElement
}
class BarMenu extends React.Component<any,any> {

    constructor(props:any){
        super(props);
        this.state = {
            auth :true,
            anchorEl:null,
            isOpen:this.props.open
        }

        this.handleChange = this.handleChange.bind(this);
        this.handleMenu = this.handleMenu.bind(this);
        this.handleClose = this.handleClose.bind(this);
        this.handleDrawer = this.handleDrawer.bind(this);
        this.handleToken = this.handleToken.bind(this);
        this.handleLogout = this.handleLogout.bind(this);
    }


    handleChange(auth:any){
        this.setState({
            auth:auth
        });
    }

    handleMenu (anchorEl:any){
        this.setState({
            anchorEl:anchorEl
        });
    }

    handleClose(){
        this.setState({
            anchorEl:null
        });
    };

    handleDrawer(){
        this.props.openDrawer(!this.props.open);        
        /*this.setState({
            isOpen:!this.state.isOpen
        })*/
    }

    handleToken(){
        if(this.props.token==""){
            return "Login";
        }else{
            /*authent.getProfile(this.props.token).then((resolve)=>{
                return resolve.json();
            }).then((valeur)=>{
                return (
                    <>
                        {valeur.username}
                        <LogoutIcon onClick={this.handleLogout}>
                            <AccountCircle />
                        </LogoutIcon>
                    </>
                    
                );
            });*/
            return "Test";

        }
    }

    handleLogout(){
        authent.deconnexion(this.props.token).then((resolve)=>{
            return resolve.json();
        }).then((valeur)=>{
            if(valeur.success == "true"){
                return(this.props.logout(""));
            }
            
        });
    }

    render(){
        return (
            <>
            <Box sx={{ flexGrow: 1 }}>
            <AppBar position="static">
                <Toolbar>
                <IconButton size="large" edge="start" color="inherit" aria-label="menu" sx={{ mr: 2 }} onClick={this.handleDrawer}>
                    <MenuIcon />
                </IconButton>
                <Typography variant="h6" component="div" sx={{ flexGrow: 1 }}>
                    Application
                </Typography>
                {this.state.auth && (
                    <div>
                    {/*icone profile, handlemenu ouvre le popup du profil*/}
                    {this.handleToken()}
                    
                    <Menu
                        id="menu-appbar"
                        anchorEl={this.state.anchorEl}
                        anchorOrigin={{
                        vertical: 'top',
                        horizontal: 'right',
                        }}
                        keepMounted
                        transformOrigin={{
                        vertical: 'top',
                        horizontal: 'right',
                        }}
                        open={Boolean(this.state.anchorEl)}
                        onClose={this.handleClose}
                    >
                        <MenuItem onClick={this.handleClose}>Profile</MenuItem>
                        <MenuItem onClick={this.handleClose}>My account</MenuItem>
                    </Menu>
                    </div>
                )}
                </Toolbar>
            </AppBar>
            </Box>
            </>
        );
    }
}

export default BarMenu;