import * as React from 'react';
import ErrorBar from "./ErrorBar";
import Inscription from './Inscription';
import Menu from "./Menu";
import Annonce from './Annonce';

class Page extends React.Component<any,any>{

    constructor(props:any){
        super(props);
        this.state = {
            token:"",
            isConnected:false,
            snackMessage:[],
        }
        this.handleMessage = this.handleMessage.bind(this);
        this.closeMessage = this.closeMessage.bind(this);
        this.handleToken = this.handleToken.bind(this);
        this.handleLogout = this.handleLogout.bind(this);
    }

    handleMessage(message:Array<String>){
        
        this.setState({
            snackMessage:message,
        });

    }

    handleToken(token:String){
        this.setState({
            token:token
        })
    }

    closeMessage(close:boolean){
        this.setState({
            snackMessage:[],
        });

    }

    handleLogout(){
        this.setState({
            token:""
        })
    }

    render(){
        return(
            <>            
                <Menu token={this.state.token} logout={this.handleLogout}></Menu>
                <ErrorBar snackMessage={this.state.snackMessage} stopMessage={this.closeMessage}></ErrorBar>
                
                <div className="row">  
                <div className="col-3"></div>
                
                {this.state.token!=""
                    ?<Annonce token={this.state.token}></Annonce>
                    :<Inscription snackMessage={this.handleMessage} token={this.handleToken}></Inscription>
                }
                
                
                <div className="col-3"></div>
                </div>
            </>
        )
    }

}
export default Page;