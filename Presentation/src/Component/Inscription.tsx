import * as React from 'react';
import * as authent from '../Services/AuthentificationAPI.js';
import Box from '@mui/material/Box';
import Input from '@mui/material/Input';
import InputGroup from 'react-bootstrap/InputGroup';
import FormControl from 'react-bootstrap/FormControl'
import * as testapi from '../Services/TestAPI.js';
import { ThermostatOutlined } from '@mui/icons-material';

class Inscription extends React.Component<any,any>{

    constructor(props:any){
        super(props);
        this.state = {
            auth : false,
            inscriptionPseudo : "",
            inscriptionMdp : "",
            inscriptionPrenom : "",
            inscriptionNom : "",
            Pseudo : "",
            Mdp : "",
        }
        this.add = this.add.bind(this);
        this.connect = this.connect.bind(this);
        this.handleChange = this.handleChange.bind(this);

    }

    add(){
        if(!this.state.inscriptionMdp || !this.state.inscriptionNom || !this.state.inscriptionPrenom || !this.state.inscriptionPseudo){
            let listmessage = [];
            if(!this.state.inscriptionMdp){
                listmessage.push("Mot de passe manquant");
            }
            if(!this.state.inscriptionPseudo){
                listmessage.push("Pseudo manquant");
            }            
            if(!this.state.inscriptionNom){
                listmessage.push("Nom manquant");
            }
            if(!this.state.inscriptionPrenom){
                listmessage.push("Prenom manquant");
            }  
            this.props.snackMessage(listmessage);      
        }
        else{
            authent.inscription(this.state.inscriptionPseudo,this.state.inscriptionMdp,this.state.inscriptionPrenom,this.state.inscriptionNom).then((valeur)=>{this.props.token(valeur.token);});
        }
    }

    connect(){
        let listmessage = [];
        if(!this.state.Mdp || !this.state.Pseudo){
            if(!this.state.Mdp){
                listmessage.push("Mot de passe manquant");
            }
            if(!this.state.Pseudo){
                listmessage.push("Pseudo manquant");
            }
            this.props.snackMessage(listmessage);
        }
        else{
            authent.login(this.state.Pseudo,this.state.Mdp).then((valeur)=>{this.props.token(valeur.token);});       
        }
    }

    handleChange(event:any){
        const target = event.target;
        const value = target.type === 'checkbox' ? target.checked : target.value;
        const name = target.name;
        this.setState({
          [name]: value
        });
    }

    render(){
        return(
            <>
            <div className=' mx-auto col-6 row mt-3 border border-2'>
                <div className="col-6 my-3 border-end">
                        <h3 className='text-center'>Inscription</h3>
                        <div className="my-2 d-flex col-10 mx-auto">
                            <InputGroup.Text id="basic-addon1" className="bg-primary bg-gradient rounded-start rounded-0 white">Username</InputGroup.Text>
                            <FormControl
                            name= "inscriptionPseudo"
                            placeholder="Username"
                            aria-label="Username"
                            aria-describedby="basic-addon1"
                            className="rounded-end rounded-0"
                            value={this.state.inscriptionPseudo}
                            onChange={this.handleChange}
                            />
                        </div>
                        <div className="my-2 d-flex col-10 mx-auto">
                            <InputGroup.Text id="basic-addon1" className="bg-primary bg-gradient rounded-start rounded-0 white">Mot de passe</InputGroup.Text>
                            <FormControl
                            name= "inscriptionMdp"
                            placeholder="Mot de passe"
                            aria-label="Mot de passe"
                            aria-describedby="basic-addon1"
                            className="rounded-end rounded-0"
                            value={this.state.inscriptionMdp}
                            onChange={this.handleChange}
                            />
                        </div>
                        <div className="my-2 d-flex col-10 mx-auto">
                            <InputGroup.Text id="basic-addon1" className="bg-primary bg-gradient rounded-start rounded-0 white">Prénom</InputGroup.Text>
                            <FormControl
                            name= "inscriptionPrenom"
                            placeholder="Prenom"
                            aria-label="Prenom"
                            aria-describedby="basic-addon1"
                            className="rounded-end rounded-0"
                            value={this.state.inscriptionPrenom}
                            onChange={this.handleChange}
                            />
                        </div>
                        <div className="my-2 d-flex col-10 mx-auto">
                            <InputGroup.Text id="basic-addon1" className="bg-primary bg-gradient rounded-start rounded-0 white">Nom</InputGroup.Text>
                            <FormControl
                            name= "inscriptionNom"
                            placeholder="Nom"
                            aria-label="Nom"
                            aria-describedby="basic-addon1"
                            className="rounded-end rounded-0"
                            value={this.state.inscriptionNom}
                            onChange={this.handleChange}
                            />
                        </div>

                        <button className='mx-auto my-2 btn btn-primary d-flex' onClick={this.add}>Valider</button>
                </div>
                <div className="col-6 mt-3">
                        <h3 className='text-center'>Connexion</h3>
                        <div className="my-2 d-flex col-10 mx-auto">
                            <InputGroup.Text id="basic-addon1" className="bg-primary bg-gradient rounded-start rounded-0 white">Username</InputGroup.Text>
                            <FormControl
                            name= "Pseudo"
                            placeholder="Username"
                            aria-label="Username"
                            aria-describedby="basic-addon1"
                            className="rounded-end rounded-0"
                            value={this.state.Pseudo}
                            onChange={this.handleChange}
                            />
                        </div>
                        <div className="my-2 d-flex col-10 mx-auto">
                            <InputGroup.Text id="basic-addon1" className="bg-primary bg-gradient rounded-start rounded-0 white">Mot de passe</InputGroup.Text>
                            <FormControl
                            name= "Mdp"
                            placeholder="Mot de passe"
                            aria-label="Mot de passe"
                            aria-describedby="basic-addon1"
                            className="rounded-end rounded-0"
                            value={this.state.Mdp}
                            onChange={this.handleChange}
                            />
                        </div>
                        <button className='mx-auto my-2 btn btn-primary d-flex' onClick={this.connect}>Connexion</button>
                </div>
            </div>
            </>
        );
    }

}
export default Inscription;