import * as React from 'react';
import * as publication from '../Services/PublicationAPI.js';
import Dialog from '@mui/material/Dialog';
import DialogTitle from '@mui/material/DialogTitle';
import { ThemeContext } from '@emotion/react';
import BookmarkBorderIcon from '@mui/icons-material/BookmarkBorder';
import ArrowRightIcon from '@mui/icons-material/ArrowRight';
import DeleteOutlineIcon from '@mui/icons-material/DeleteOutline';
import EditIcon from '@mui/icons-material/Edit';
import VisibilityIcon from '@mui/icons-material/Visibility';
class Annonce extends React.Component<any,any>{

    constructor(props:any){
        super(props);
        this.state = {
            listPublication : null,
            open:false,
            isEditing:false,
            title:"",
            description:"",
            id:"",
            path:"",
            type:""
        }
        this.handleDelete = this.handleDelete.bind(this);
        this.handleUpdate = this.handleUpdate.bind(this);
        this.handleAdd = this.handleAdd.bind(this);
        this.handleClose = this.handleClose.bind(this);
        this.handleOpenUpdate = this.handleOpenUpdate.bind(this);
        this.handleOpenNew = this.handleOpenNew.bind(this);
        this.handleChange = this.handleChange.bind(this);
        this.typeButton = this.typeButton.bind(this);
        this.typetitle = this.typetitle.bind(this);
        this.handleGet = this.handleGet.bind(this);
    }

    handleOpenNew(e:any){
        this.setState({
            open:true,
            title:"",
            id:"",
            description:"",
            path:"",
            type:"new"
        });           
    }

    handleOpenUpdate(e:any){
        this.setState({
            open:true,
            title:e.target.dataset.title,
            id:e.target.dataset.id,
            description:e.target.dataset.desc,
            path:e.target.dataset.path,
            type:"update"
        });           
    }

    handleClose(){
        this.setState({
            open:false,
            title:"",
            description:"",
            id:"",
            path:"",
            type:""
        });
    }



    handleDelete(e:any){
        publication.Delete(e.currentTarget.value,this.props.token);
    }

    handleUpdate(){
        publication.Update(this.state.title,this.state.description,this.state.path,this.state.id,this.props.token);
    }

    handleAdd(){
        publication.Add(this.state.title,this.state.description,this.state.path,this.props.token);
    }

    handleGet(e:any){
        publication.GetPublication(e.currentTarget.value,this.props.token).then((valeur)=>{
            return valeur.json();
        }).then((element)=>{
            console.log(element);
            this.setState({
                title:element.publication.title,
                description:element.publication.description,
                id:element.publication.id,
                path:element.publication.id,
                open:true,
                type:"view"
            });
        });

    }

    componentDidMount(){
        let listPublication = publication.getAllPublication(this.props.token).then((resolve) =>{
            return resolve.json();
        }).then((valeur)=>{
            console.log(valeur);
            let listhtml = [];
            let list = valeur.publications.map((element:any,i:any) => {
                return(<div className="col card border border-2 mt-3 mx-1 p-0 rounded h-50" key={element._id}>
                    <h3 className="bleu card-header d-flex h-25"><BookmarkBorderIcon className='mx-2 mt-1'/>{element.title}</h3>
                    <div className="my-4 mx-2 overflow-hidden inherit card-body">{element.description}</div>
                    <div className="card-body d-flex">
                        <button className="mt-auto bg-primary btn mx-2 white" value={element._id} onClick={this.handleDelete}>< DeleteOutlineIcon/></button>
                        <button className="mt-auto bg-primary btn mx-2 white" data-id={element._id} data-title={element.title} data-desc={element.description} data-path={element.path} onClick={this.handleOpenUpdate}><EditIcon/></button>
                        <button className="mt-auto bg-primary btn mx-2 white" value={element._id} onClick={this.handleGet}><VisibilityIcon/></button>
                    </div>
                   
                </div>)
            });
            let listvaleur = [];
            for(let i=0;i<list.length;i++){
                listvaleur.push(list[i]);
                if((i+1)%5==0){
                    listhtml.push(listvaleur);
                    listvaleur= [];
                }
                if(i+1 == list.length){
                    listhtml.push(listvaleur);
                }
            }
            let listfinale = listhtml.map((element,i) => {
                return(<div className="col-10 row mx-auto" key={i}>
                        {element}
                    </div>)
            });
            this.setState({
                listPublication:listfinale
            });
            return listfinale;
        });
        
    }

    handleChange(event:any){
        const target = event.target;
        const value = target.type === 'checkbox' ? target.checked : target.value;
        const name = target.name;
        this.setState({
          [name]: value
        });
    }

    typeButton(){
        if(this.state.type=="update"){
            return(<div className='d-flex'>
                        <button className='bg-primary m-2 btn white'  data-id={this.state.id} data-path={this.state.path}onClick={this.handleUpdate}>Update</button>
                        <button className='bg-primary m-2 btn white' onClick={this.handleClose}>Annuler</button>
                    </div>)
        }
        else if(this.state.type=="new"){
            return(<div className='d-flex'>
                        <button className='bg-primary m-2 btn white' onClick={this.handleAdd}>Ajout</button>
                        <button className='bg-primary m-2 btn white' onClick={this.handleClose}>Annuler</button>
            </div>)
        }else{
            return(<div className='d-flex'>
            <button className='bg-primary m-2 btn white' onClick={this.handleClose}>Annuler</button>
            </div>)
        }
    }

    typetitle(){
        if(this.state.type == "update"){
            return "Mettre à jour la publication";
        }
        else if(this.state.type == "new"){
            return "Ajouter une publication";
        }else{
            return "Voir une publication";
        }
    }

    typeDialog(){  
        console.log(this.state.description);
        if(this.state.type != "view"){
            return(
            <>            
                <div className='input-group w-75 m-auto'>
                    <span className="input-group-text" id="inputGroup-sizing-sm">Titre</span>
                    <input type="text" value={this.state.title} className="form-control" onChange={this.handleChange} name="title"></input>
                </div>

                <input type="text" value={this.state.description} className="form-control my-2 mx-auto w-90" onChange={this.handleChange} name="description"></input>
                            
                {this.typeButton()}
            </>
            );

        }else{
            return(
            <>            
                <div className='w-75 m-auto'>
                    <div>{this.state.title}</div>
                </div>

                <div className="my-2 mx-auto w-90">{this.state.description}</div>

                {this.typeButton()}
            </>
            );
                    
        }

    }
    render(){
        return(
            <>
                <Dialog fullWidth maxWidth="sm" open={this.state.open} className="m-0">
                <div className='fs-3 bold mx-2'>{this.typetitle()}</div>
                    {this.typeDialog()}

                </Dialog>
                <div className="row">
                    <div className="col-1"></div>
                    <div className="row text-start col-9 mt-3">
                        <h1>Annonce</h1>
                        <hr/>
                    </div>
                    <div className="col-2 my-auto">
                        <button className='btn bg-primary white my-auto btn-lg mx-5' onClick={this.handleOpenNew}>Ajouter</button>
                    </div>
                </div>
                {this.state.listPublication}        
            </>
        )
    }
}
export default Annonce;