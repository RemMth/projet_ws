import { MenuList } from '@mui/material';
import * as React from 'react';
import BarMenu from './BarMenu';
import DrawerMenu from './DrawerMenu';


class Menu extends React.Component<any,any>{
    constructor(props:any){
        super(props);
        this.state = {
            isOpen:false
        }

        this.handleDrawer = this.handleDrawer.bind(this);
        this.handleLogout = this.handleLogout.bind(this);
    }

    handleDrawer(openDrawer:boolean){
        this.setState({
            isOpen:openDrawer
        });
    }

    handleLogout(){
        this.props.logout("");
    }

    

    render(){
        return(
            <>
            <BarMenu openDrawer={this.handleDrawer} open={this.state.isOpen} token={this.props.token} logout={this.handleLogout}></BarMenu>
            <DrawerMenu open={this.state.isOpen} close={this.handleDrawer} token={this.props.token}></DrawerMenu>
            </>
        )
    }
}

export default Menu;