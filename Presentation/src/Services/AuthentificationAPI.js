const API = "http://localhost:8080/users";

/*  headers.append("Access-Control-Allow-Origin", "http://localhost:3000");
    headers.append('Access-Control-Allow-Credentials', 'true');
    headers.append('Accept', 'application/json');
    headers.append('Access-Control-Allow-Methods',"POST");
    fetch(API+"/login", {method:"POST",
                        body:JSON.stringify(body),
                        headers: headers}).then(response => response.json()).then(json => console.log(json));*/

/*export function confirmConnexion(){
    console.log(API);
    fetch(API+"/me").then(function(response){
        return response.json();
    }).then(function(data){
        return data;
    });
}
*/

//sur inscription vers app
//fait
export function login(username,password){
    let body = {
        "username": username,
        "password": password    
    }
    let headers= new Headers();
    headers.append("Content-type","application/json");
    return (async () => {
        const rawResponse = await fetch(API+"/login", {method:"POST",
                        body:JSON.stringify(body),
                        headers: headers});
        return await rawResponse.json();
    })();
}

//sur inscription vers app
//fait
export function inscription(username,password,firstName,lastName){
    let body = {
        "username": username,
        "password": password,
        "firstName": firstName,
        "lastName": lastName
    } 
    let headers= new Headers();
    headers.append("Content-type","application/json");
    return (async () => {
        const rawResponse = await fetch(API+"/signup", {method:"POST",
                        body:JSON.stringify(body),
                        headers: headers});
        return await rawResponse.json();
    })();
}

//sur app je sais pas les condition
export function refresh(token){
    let body = {
        "token": token,
    } 
    let headers= new Headers();
    headers.append("Content-type","application/json");
    return (async () => {
        const rawResponse = await fetch(API+"/refreshToken", {method:"POST",
                        body:JSON.stringify(body),
                        headers: headers});
        return await rawResponse;
    })();
}

//sur menu token sur app 
//fait
export function getProfile(token){
    let body = {
        "token": token,
    } 
    let headers= new Headers();
    headers.append("Content-type","application/json");
    return (async () => {
        const rawResponse = await fetch(API+"/me", {method:"POST",
                        body:JSON.stringify(body),
                        headers: headers});
        return await rawResponse;
    })();
}

//sur le menu token sur app  envoie vide a app
//fait
export function deconnexion(token){
    let body = {
        "token": token,
    } 
    let headers= new Headers();
    headers.append("Content-type","application/json");
    return (async () => {
        const rawResponse = await fetch(API+"/logout", {method:"POST",
                        body:JSON.stringify(body),
                        headers: headers});
        return await rawResponse;
    })();
}