const API = "http://localhost:8080/publications";



export function getAllPublication(token){
    let headers= new Headers();
    headers.append("Content-type","application/json");
    headers.append('Authorization', 'Bearer ' + token);
    return (async () => {
        const rawResponse = await fetch(API, {method:"GET",
                        headers: headers});
        return await rawResponse;
    })();

}


export function Delete(id,token){
    if(id){
        let headers= new Headers();
        headers.append("Content-type","application/json");
        headers.append('Authorization', 'Bearer ' + token);
        return (async () => {
            const rawResponse = await fetch(API+"/delete/"+id, {method:"DELETE",
                            headers: headers});
            return await rawResponse;
        })();  
    }

}

export function Update(title,description,path,id,token){
    if(id){
        let body = {
            "title": title,
            "description":description,
            "path":path
        } 
        let headers= new Headers();
        headers.append("Content-type","application/json");
        headers.append('Authorization', 'Bearer ' + token);
        return (async () => {
            const rawResponse = await fetch(API+"/update/"+id, {method:"PUT",
                            body:JSON.stringify(body),
                            headers: headers});
            return await rawResponse;
        })();
    }

}

export function Add(title,description,path,token){
        let body = {
            "title": title,
            "description":description,
            "path":path
        } 
        let headers= new Headers();
        headers.append("Content-type","application/json");
        headers.append('Authorization', 'Bearer ' + token);
        return (async () => {
            const rawResponse = await fetch(API+"/new", {method:"POST",
                            body:JSON.stringify(body),
                            headers: headers});
            return await rawResponse;
        })();
}

export function GetPublication(id,token){
    let headers= new Headers();
    headers.append("Content-type","application/json");
    headers.append('Authorization', 'Bearer ' + token);
    return (async () => {
        const rawResponse = await fetch(API+"/"+id, {method:"GET",
                        headers: headers});
        return await rawResponse;
    })();   
}